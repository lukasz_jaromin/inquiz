package com.jarmin.InQuiz.service;

import com.jarmin.InQuiz.dto.SaveUserDto;
import com.jarmin.InQuiz.entity.RegisteredUser;
import com.jarmin.InQuiz.mapper.UserMapper;
import com.jarmin.InQuiz.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private final PasswordEncoder passwordEncoder;

    @Override
    public void registerUser(SaveUserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        RegisteredUser user = userMapper.mapSaveUserDtoToEntity(userDto);
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        RegisteredUser registeredUser = userRepository.findByEmail(email);
        return Optional.ofNullable(registeredUser)
                .map(MyUserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("Brak użytkownika o adresie email: " + email));
    }

    @RequiredArgsConstructor
    private static class MyUserDetails implements UserDetails {

        private final RegisteredUser registereduser;

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return null;
        }

        @Override
        public String getPassword() {
            return registereduser.getPassword();
        }

        @Override
        public String getUsername() {
            return registereduser.getEmail();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
