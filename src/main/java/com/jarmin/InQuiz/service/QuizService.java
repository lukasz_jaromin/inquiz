package com.jarmin.InQuiz.service;

import com.jarmin.InQuiz.dto.SaveQuizDto;

public interface QuizService {

    void saveQuiz(SaveQuizDto quiz);

    void deleteQuiz(int id);

    void editQuiz(SaveQuizDto saveQuizDto, int id);
}
