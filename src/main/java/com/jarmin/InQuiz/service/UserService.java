package com.jarmin.InQuiz.service;

import com.jarmin.InQuiz.dto.SaveUserDto;

public interface UserService {

    void registerUser(SaveUserDto user);
}
