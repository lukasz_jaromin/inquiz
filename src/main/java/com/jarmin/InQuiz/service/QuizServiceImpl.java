package com.jarmin.InQuiz.service;

import com.jarmin.InQuiz.dto.SaveQuizDto;
import com.jarmin.InQuiz.entity.Quiz;
import com.jarmin.InQuiz.mapper.QuizMapper;
import com.jarmin.InQuiz.repository.QuizRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class QuizServiceImpl implements QuizService {

    private final QuizRepository quizRepository;
    private final QuizMapper quizMapper = Mappers.getMapper(QuizMapper.class);

    @Override
    public void saveQuiz(SaveQuizDto saveQuizDto) {
        Quiz quiz = quizMapper.mapSaveQuizDtoToEntity(saveQuizDto);
        quizRepository.save(quiz);
    }

    @Override
    public void deleteQuiz(int id) {
        quizRepository.deleteById(id);
    }

    @Override
    public void editQuiz(SaveQuizDto saveQuizDto, int id) {
        Quiz quiz = quizMapper.mapSaveQuizDtoToEntity(saveQuizDto);
        quiz.setId(id);
        quizRepository.save(quiz);
    }


}
