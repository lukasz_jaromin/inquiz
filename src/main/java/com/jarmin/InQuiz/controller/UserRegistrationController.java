package com.jarmin.InQuiz.controller;


import com.jarmin.InQuiz.dto.SaveUserDto;
import com.jarmin.InQuiz.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserRegistrationController {

    private final UserService userService;

    @GetMapping("/test")
    @ResponseBody
    public String getTest(@RequestParam(defaultValue = "test") String id) {
        return "ID: " + id;
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public String register(@RequestBody SaveUserDto saveUserDto) {
        userService.registerUser(saveUserDto);
        return "User " + saveUserDto.getEmail() + " registered successfully";
    }
}
