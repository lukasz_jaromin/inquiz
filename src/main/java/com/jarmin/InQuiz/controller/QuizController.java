package com.jarmin.InQuiz.controller;

import com.jarmin.InQuiz.dto.SaveQuizDto;
import com.jarmin.InQuiz.service.QuizService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/quizes")
public class QuizController {

    private final QuizService quizService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String saveQuiz(@RequestBody SaveQuizDto saveQuizDto) {
        quizService.saveQuiz(saveQuizDto);
        return "Quiz: " + saveQuizDto.getQuestion() + " added to the database.";
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String deleteQuiz(@PathVariable int id){
        quizService.deleteQuiz(id);
        return "Quiz for the given id: " + id + "has been removed.";
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String editQuiz(@RequestBody SaveQuizDto saveQuizDto, @PathVariable("id") int id ){
        quizService.editQuiz(saveQuizDto, id);
        return "Quiz for the given id: " + id + "has been edited.";
    }

}
