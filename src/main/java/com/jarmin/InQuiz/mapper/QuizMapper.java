package com.jarmin.InQuiz.mapper;

import com.jarmin.InQuiz.dto.SaveQuizDto;
import com.jarmin.InQuiz.entity.Quiz;
import org.mapstruct.Mapper;

@Mapper
public interface QuizMapper {

    Quiz mapSaveQuizDtoToEntity(SaveQuizDto saveQuizDto);
}
