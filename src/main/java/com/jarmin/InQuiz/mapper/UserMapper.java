package com.jarmin.InQuiz.mapper;

import com.jarmin.InQuiz.dto.SaveUserDto;
import com.jarmin.InQuiz.entity.RegisteredUser;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    RegisteredUser mapSaveUserDtoToEntity(SaveUserDto saveUserDto);
}
