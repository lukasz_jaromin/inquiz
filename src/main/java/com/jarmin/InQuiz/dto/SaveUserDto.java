package com.jarmin.InQuiz.dto;

import lombok.Data;

@Data
public class SaveUserDto {

    private String firstName;

    private String password;

    private String email;
}
