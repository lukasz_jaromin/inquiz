package com.jarmin.InQuiz.dto;

import lombok.Data;

@Data
public class SaveQuizDto {

    private String question;

    private String answer;
}
