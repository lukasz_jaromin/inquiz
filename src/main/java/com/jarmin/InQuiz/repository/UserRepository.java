package com.jarmin.InQuiz.repository;

import com.jarmin.InQuiz.entity.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<RegisteredUser, Integer> {

    RegisteredUser findByEmail(String email);
}
