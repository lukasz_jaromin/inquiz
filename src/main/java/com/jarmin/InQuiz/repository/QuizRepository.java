package com.jarmin.InQuiz.repository;

import com.jarmin.InQuiz.entity.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {
}
