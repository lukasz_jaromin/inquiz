package com.jarmin.InQuiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InQuizApplication {

    public static void main(String[] args) {
        SpringApplication.run(InQuizApplication.class, args);
    }
}
