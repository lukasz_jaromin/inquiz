package com.jarmin.InQuiz.service

import com.jarmin.InQuiz.dto.SaveQuizDto
import com.jarmin.InQuiz.entity.Quiz
import com.jarmin.InQuiz.mapper.QuizMapper
import com.jarmin.InQuiz.repository.QuizRepository
import spock.lang.Specification

class QuizServiceImplTest extends Specification {

    private static final int ID = 1
    private static final String QUESTION = "question"
    private static final String ANSWER = "answer"
    private static final String NEW_QUESTION = "new question"
    private static final String NEW_ANSWER = "new answer"

    QuizRepository quizRepository = Mock()
    QuizMapper quizMapper = Mock()

    QuizServiceImpl subject = new QuizServiceImpl(quizRepository)

    def "saveQuiz should save question and answer into the database"() {

        given:
        Quiz quiz = new Quiz(question: QUESTION, answer: ANSWER)
        SaveQuizDto saveQuizDto = new SaveQuizDto(question: QUESTION, answer: ANSWER)


        when:
        subject.saveQuiz(saveQuizDto)

        then:
        1 * quizRepository.save(quiz)

    }

    def "deleteQuiz should delete quiz by id from database"() {

        given:
        Quiz quiz = new Quiz(id: ID, question: QUESTION, answer: ANSWER)

        when:
        subject.deleteQuiz(ID)

        then:
        1 * quizRepository.deleteById(ID)
    }

    def "saveEditedQuiz should save changes for given id"() {

        given:
        Quiz quizWithId = new Quiz(id: ID, question: NEW_QUESTION, answer: NEW_ANSWER)
        Quiz quizWithoutId = new Quiz(question: NEW_ANSWER, answer: NEW_ANSWER)
        SaveQuizDto saveQuizDto = new SaveQuizDto(question: NEW_QUESTION, answer: NEW_ANSWER)
        quizMapper.mapSaveQuizDtoToEntity(saveQuizDto) >> quizWithoutId

        when:
        subject.editQuiz(saveQuizDto, ID)

        then:
        1 * quizRepository.save(quizWithId)
    }
}
