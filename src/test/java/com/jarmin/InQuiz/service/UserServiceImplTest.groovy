package com.jarmin.InQuiz.service

import com.jarmin.InQuiz.dto.SaveUserDto
import com.jarmin.InQuiz.entity.RegisteredUser
import com.jarmin.InQuiz.mapper.UserMapper
import com.jarmin.InQuiz.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

class UserServiceImplTest extends Specification {

    private static final int ID = 1
    private static final String FIRSTNAME = "Mike"
    private static final String PASSWORD = "password"
    private static final String ENCODED_PASSWORD = "encoded password"
    private static final String EMAIL = "mike@wp.pl"
    private static final String INVALIDEMAIL = "n.m"

    UserRepository userRepository = Mock(UserRepository)
    PasswordEncoder passwordEncoder = Mock(PasswordEncoder)
    UserMapper userMapper = Mock()

    UserServiceImpl underTest = new UserServiceImpl(userRepository, passwordEncoder)

    def "RegisterUser"() {

        given:
        RegisteredUser user = new RegisteredUser(firstName: FIRSTNAME, password: ENCODED_PASSWORD, email: EMAIL)
        SaveUserDto saveUserDto = new SaveUserDto(firstName: FIRSTNAME, password: PASSWORD, email: EMAIL)
        passwordEncoder.encode(saveUserDto.getPassword()) >> ENCODED_PASSWORD
        userMapper.mapSaveUserDtoToEntity(saveUserDto) >> user

        when:
        underTest.registerUser(saveUserDto)

        then:
        1 * userRepository.save(user)
    }

    def "UserServiceImpl should return UserDetails based on username"() {

        given:
        RegisteredUser user1 = new RegisteredUser(id: ID, firstName: FIRSTNAME, password: PASSWORD, email: EMAIL)
        userRepository.findByEmail(EMAIL) >> user1

        when:
        UserDetails userFormService = underTest.loadUserByUsername(EMAIL)

        then:
        userFormService.username == user1.email
        userFormService.password == user1.password
        userFormService.accountNonExpired
        userFormService.accountNonLocked
        userFormService.credentialsNonExpired
        userFormService.enabled
    }

    def "LoadUserByUsername when email doesn't exist should thrown exeption"() {

        when:
        underTest.loadUserByUsername(INVALIDEMAIL)

        then:
        UsernameNotFoundException exception = thrown()
        exception.message == "Brak użytkownika o adresie email: " + INVALIDEMAIL
    }
}
